
function scroll_to_class(element_class, removed_height) {
    var scroll_to = $(element_class).offset().top - removed_height;
    if($(window).scrollTop() != scroll_to) {
        $('html, body').stop().animate({scrollTop: scroll_to}, 0);
    }
}

function bar_progress(progress_line_object, direction) {
    var number_of_steps = progress_line_object.data('number-of-steps');
    var now_value = progress_line_object.data('now-value');
    var new_value = 0;
    if(direction == 'right') {
        new_value = now_value + ( 100 / number_of_steps );
    }
    else if(direction == 'left') {
        new_value = now_value - ( 100 / number_of_steps );
    }
    progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}

jQuery(document).ready(function() {


    /*
     Form
     */
    $('.f1 fieldset:first').fadeIn('slow');

    $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function() {
        $(this).removeClass('input-error');
    });

    // next step
    $('.f1 .btn-next').on('click', function() {
        var parent_fieldset = $(this).parents('fieldset');
        var next_step = true;
        // navigation steps / progress steps
        var current_active_step = $(this).parents('.f1').find('.f1-step.active');
        var progress_line = $(this).parents('.f1').find('.f1-progress-line');

        // fields validation
        parent_fieldset.find('input[type="text"], input[type="password"],input[type="email"], textarea, select').each(function() {
//            if( !$(this).val()) {
//                $(this).addClass('input-error');
//                next_step = false;
//            }
//            else {
//                $(this).removeClass('input-error');
//            }
        });


        // email validation
        parent_fieldset.find('input[type="email"]').each(function() {
//            if ($(this).val() != "") {
//                if (!validateEmail($(this).val())) {
//                    $(this).addClass('input-error');
//                    alert($(this).attr('placeholder') + ' is invalid.');
//                    next_step = false;
//                }
//                else {
//                    $(this).removeClass('input-error');
//                }
//            }
        });

        // phone validation
        parent_fieldset.find('input#f1-phone').each(function() {
//            if ($(this).val() != "") {
//                if (!validatePhone($(this).val())) {
//                    $(this).addClass('input-error');
//                    alert($(this).attr('placeholder') + ' is invalid. Please enter 10 digits only.');
//                    next_step = false;
//                }
//                else {
//                    $(this).removeClass('input-error');
//                }
//            }
        });


        // zip validation
        parent_fieldset.find('input#f1-address-zip').each(function() {
//            if ($(this).val() != "") {
//                if (!validateZip($(this).val(),parent_fieldset.find('#country').val())) {
//                    $(this).addClass('input-error');
//                    alert($(this).attr('placeholder') + ' is invalid. Please enter correct zip code.');
//                    next_step = false;
//                }
//                else {
//                    $(this).removeClass('input-error');
//                }
//            }
        });

        var isCheckoxesCarriers = parent_fieldset.find('input[name="carriers[]"]').length;

//        if (isCheckoxesCarriers) {
//            var atLeastOneIsChecked = parent_fieldset.find('input[name="carriers[]"]:checked').length > 0;
//            if (!atLeastOneIsChecked) {
//                $('#carriers_label').addClass('error');
//                //               alert($(this).attr('placeholder') + ' is invalid. Please enter 10 digits only.');
//                next_step = false;
//            } else {
//                $('#carriers_label').removeClass('error');
//            }
//
//        }



        // checkbox validation
        parent_fieldset.find('checkbox').each(function() {
//            if ($(this).val() != "") {
//                if (!validatePhone($(this).val())) {
//                    $(this).addClass('input-error');
//                    alert($(this).attr('placeholder') + ' is invalid. Please enter 10 digits only.');
//                    next_step = false;
//                }
//                else {
//                    $(this).removeClass('input-error');
//                }
//            }
        });

        // fields validation

        if( next_step ) {
            parent_fieldset.fadeOut(400, function() {
                // change icons
                current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                // progress bar
                bar_progress(progress_line, 'right');
                // show next step
                $(this).next().fadeIn();
                // scroll window to beginning of the form
                scroll_to_class( $('.f1'), 20 );
            });
        }

    });

    // previous step
    $('.f1 .btn-previous').on('click', function() {
        // navigation steps / progress steps
        var current_active_step = $(this).parents('.f1').find('.f1-step.active');
        var progress_line = $(this).parents('.f1').find('.f1-progress-line');

        $(this).parents('fieldset').fadeOut(400, function() {
            // change icons
            current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
            // progress bar
            bar_progress(progress_line, 'left');
            // show previous step
            $(this).prev().fadeIn();
            // scroll window to beginning of the form
            scroll_to_class( $('.f1'), 20 );
        });
    });

    // submit
    $('.f1').on('submit', function(e) {

        // fields validation
        $(this).find('input[type="text"], input[type="password"],  input[type="email"], textarea, select').each(function() {
//            if( !$(this).val() ) {
//                e.preventDefault();
//                $(this).addClass('input-error');
//            }
//            else {
//                $(this).removeClass('input-error');
//            }
        });
        // fields validation

    });

    //todo: refactor to a common function of population values
    //fields populations for setup wizard
    $('form#f1-address select#country').change(function() {
        initStateOrProvincesOptions('f1-address');
    });

    PREVIOUS_SELECTED_COUNTRY = $('form#f1-address select#country option:selected').val();
    initStateOrProvincesOptions('f1-address');


    //settings form submit buttons and validation
    var settings_form_submit_button = ' <button id="settings_form_submit" class="btn btn-success" type="button" name="button">Save changes</button>'
    var settings_form_submit_button2 = ' <button class="btn btn-success" type="button" name="button" style="float:right; margin-right:20px;" id="settings_form_submit2">Save changes</button>'

    var $settings_form = $('#shop_settings');
    var $page_submit_block1 = $('#panel-title');
    var form_valid = false;
    var submit_buttons_added = false;

    //fields populations for settings form
    if (!PREVIOUS_SELECTED_COUNTRY) {
        PREVIOUS_SELECTED_COUNTRY = $settings_form.find('select#country option:selected').val();

        if (typeof SELECTED_STATE_PROVINCE !== 'undefined') {
            PREVIOUS_SELECTED_COUNTRY = null;
            initStateOrProvincesOptions('shop_settings');
            PREVIOUS_SELECTED_COUNTRY = $settings_form.find('select#country option:selected').val();
            $settings_form.find('select#state_province').val(SELECTED_STATE_PROVINCE);
        }

        $settings_form.find('select#country').change(function() {
            initStateOrProvincesOptions('shop_settings');
            PREVIOUS_SELECTED_COUNTRY = $settings_form.find('select#country option:selected').val();
        });
    } else {
        //setup wizard form
        var $settings_form = $('#f1-address');
        if (typeof SELECTED_STATE_PROVINCE !== 'undefined') {
            PREVIOUS_SELECTED_COUNTRY = null;
            initStateOrProvincesOptions('f1-address');
            PREVIOUS_SELECTED_COUNTRY = $settings_form.find('select#country option:selected').val();
            $settings_form.find('select#state_province').val(SELECTED_STATE_PROVINCE);
        }

        $settings_form.find('select#country').change(function() {
            initStateOrProvincesOptions('f1-address');
            PREVIOUS_SELECTED_COUNTRY = $settings_form.find('select#country option:selected').val();
        });
        var $settings_form = $('#shop_settings');
    }

    $settings_form.find('input, checkbox').change(function() {
        if (!submit_buttons_added) {
            submit_buttons_added = true;
            $page_submit_block1.append(settings_form_submit_button);
            $settings_form.append(settings_form_submit_button2);
        }

        $('#settings_form_submit, #settings_form_submit2').click(function() {
            // email validation
            form_valid = true;

            $settings_form.find('input[type="email"]').each(function() {
//                if ($(this).val() != '') {
//                    if (!validateEmail($(this).val())  && $(this).val() ) {
//                        $(this).addClass('input-error');
//                        alert($(this).attr('placeholder') + ' is invalid.');
//                        form_valid = false;
//                    }
//                    else {
//                        $(this).removeClass('input-error');
//                    }
//                }
            });

            if (form_valid) {
                $settings_form.submit();
            }
        });
    });

    $('#order_filter select').change(function() {
        $('#order_filter input[name=page]').val('1');
        $('#order_filter input[name=error]').val('');
        $('#order_filter').submit();
    });

    //print lables bulk operation
    var count_checked_orders = 0;
    var orders_ids = [];

    var processChekedOrders = function() {
        count_checked_orders = $( "input:checked" ).length;

        orders_ids = [];
        $( "input:checked").each(function() {
            if ($(this).val()) {
                if (orders_ids.indexOf($(this).val()) == -1) {
                    orders_ids.push($(this).val());
                }
            }
        });

        if (count_checked_orders) {
            if(!$('#button_print_shipping_labels').hasClass('active')) {
                $('#button_print_shipping_labels').addClass('active');
            }
        } else {
            $('#button_print_shipping_labels').removeClass('active');
        }
    };
    processChekedOrders();

    $('input[name=order_ids]').on( "click", processChekedOrders );

    $('#button_print_shipping_labels').click(function () {

        var checked_orders = $('#form-orders-list input[type=checkbox]:checked');
        if (checked_orders.length > 0) {
            var freightcom_order_ids_param = orders_ids.join(',');
            var url = '/orders/print_shipping_labels?orders_ids=' + freightcom_order_ids_param;

            window.open(url);
        }
    });

    $('.button_ship_order').click(function () {
        $('#popup_loader').show();
        var order_id = $(this).data('orderId');
        var shop = $(this).data('shop');
        var url = '/orders/ajax_popup_ship_order/?order_id='+order_id+'&shop='+shop;

        $.ajax({
            type: "GET",
            url: url
        })
            .done(function (html) {
                $('#shipOrder').html(html);
                shipOrderPopupInit();
                $('#popup_loader').hide();
            });
    });

    $('#send_tracking_btn').click(function () {
        var order_id = $(this).data('orderId');
        var order_number = $(this).data('orderNumber');
        var shop = $(this).data('shop');
        var customer_email = $(this).data('customer');
        var tracking_number = $(this).data('trackingNumber');
        var url = '/orders/get_send_tracking_number_popup?shop='+shop;

        var dataIn = {
            'orderId': order_id,
            'order_number': order_number,
            'tracking_number': tracking_number,
            'customer': customer_email
        };

        $.post(url, dataIn, function (response) {
            if (response.error != undefined) {
                alert(response.error);
                return false;
            }

            $('#sendTrackingNumber').html(response.view);
            $('#sendTrackingNumber').modal('toggle');
            return true;
        }, 'json');
    });

    lockSetupFormSubmitByEnterKey();

    $('#product_import_submit').click(function () {
        var form = document.querySelector('form#product-import');
        form.submit();
    });

    /*$('#product_import_submit').click(function () {
     var shop = $(this).data('shop');
     var url = '/products/products_import/?shop='+shop;
     var form = document.querySelector('form#product-import');
     var fd = new FormData(form);
     //console.log(fd);

     console.log('Form send');

     $.ajax({
     url: url,
     type: "POST",
     data: fd,
     dataType: 'json',
     contentType: 'multipart/form-data',
     processData: false,
     success: function (response) {
     console.log(response);
     }
     });
     });*/
});

function shipOrderPopupInit() {
    alert(1);
    //shipp package form
    var $selected_ship_package_option = $('form#ship_order select#shipping_package option:selected');
    var ship_package_id = $selected_ship_package_option.val();
    var ship_package_weight = $selected_ship_package_option.data('weight');

    if (ship_package_weight) {
        $('form#ship_order input#shipping_package_weight').val(ship_package_weight);
    }

    updateRatesInit();
    shipOrderFormChangedInit();
    shippingPackInit();
    shippingQuotePackagesInit();
    shippingCustomBoxInit();
    //updateRatesAction();

    $('#ship_order_submit').click(function() {

        var $form = $('#ship_order');
        var data = $form.serialize();
        var url = '/orders/ajax_popup_ship_order_save/';

        var weight = $('#shipping_package_weight').val();
        var package_id = $('#shipping_package').val();
        var rate = $('input[name=rate]:checked', '#ship_order').val();

        var use_quote_packages = $("input[name=use_quote_packages]").is(':checked');

        if (!use_quote_packages) {
            if (rate == 'undefined' || !rate)  {
                return false;
            }

            if (!weight || !package_id) {
                return false;
            }
        }

        $('#popup_loader').show();
//
//        if ($(this).hasClass('btn-success') == false) {
//            alert('Please select an option of Rates!');
//            return false;
//        }

        $.ajax({
            type: "POST",
            url: url,
            data: data
        }).done(function(data) {
                if ("true" === data) {
                    window.location.reload();
                } else {
                    alert('Undefined error! Please check the form and try again!');
                    $('#popup_loader').hide();
                }
            });
    });
}

function shippingPackInit() {
    $('#shipping_package').change(function() {
        if ($(this).val() == -1) {//if pack selected
            $('#shipping_package_insurance').hide();
            $('#shipping_package_insurance').val('');
        } else {
            $('#shipping_package_insurance').show();
        }
    });
}

function shippingQuotePackagesInit() {
    initSelectPackagesFields();
    $('input[name=use_quote_packages]').change(function() {
        initSelectPackagesFields();
    });
}

function initSelectPackagesFields() {
    var quote_selected_carrier = $("input[name='quote_selected_carrier']").val();
    var quote_selected_service = $("input[name='quote_selected_service']").val();
    var use_quote_packages = $("input[name=use_quote_packages]").is(':checked');


    if (use_quote_packages && quote_selected_carrier && quote_selected_service == 'Freightcom') {
        hideSelectPackageFields();

        $('#ship_order_submit').addClass('btn-success');
        $("#quote_package_selector").show();

    } else {
        showSelectPackageFields();
        $('#ship_order_submit').removeClass('btn-success');
        if (quote_selected_service != 'Freightcom') {
            $("input[name=use_quote_packages]").prop("checked",false);
            $("#quote_package_selector").hide();



        } else {

        }
    }
}

function showSelectPackageFields() {
    $('#manual_package_selector').show();
    $('#manual_carrier_selector').show();
}

function hideSelectPackageFields() {
    $('#manual_package_selector').hide();
    $('#manual_carrier_selector').hide();
}

function shippingCustomBoxInit() {
    $('#shipping_package').change(function() {
        if ($(this).val() == -2) {//if custom box selected

//            $('#shipping_package_length').val('');
//            $('#shipping_package_width').val('');
//            $('#shipping_package_height').val('');

            $('#custom_box_dimensions').show();
        } else {
            $('#custom_box_dimensions').hide();
        }
    });
}

function shipOrderFormChangedInit() {
    $('#rates_container').html('<h5>New Shiping Rates are available for this shipment</h5><button type="button" id="update_rates" class="btn btn-default btn-update-rates my-1">Refresh Rates</button>');
    $('#update_rates').show();
    $('form#ship_order input, form#ship_order select').change(function() {
        $('#rates_container').html('<h5>New Shiping Rates are available for this shipment</h5><button type="button" id="update_rates" class="btn btn-default btn-update-rates my-1">Refresh Rates</button>');
        $('#update_rates').show();
    });
}

function updateRatesInit() {
    $('#update_rates').click(function() {
        $('#popup_loader').show();
        updateRatesAction();
    });
}

function updateRatesAction() {
    $('#popup_loader').show();
    $('#update_rates').hide();
    $('#rates_container').html('Updating...');
    $('#ship_order_submit').removeClass('btn-success');

    var $form = $('#ship_order');
    var data = $form.serialize();
    //   var $selected_ship_package_option = $('form#ship_order select#shipping_package option:selected');
//    var ship_package_id = $selected_ship_package_option.val();
//    var ship_package_weight = $('form#ship_order input#shipping_package_weight').val();
//    var ship_package_insurance = $('form#ship_order input#shipping_package_insurance').val();

   //ar url = '/orders/ajax_popup_ship_order_rates/';
    var url = 'rates_popup_data.html';


    $.ajax({
        type: "POST",
        url: url,
        data: data
    })
        .done(function (result) {
            $('#rates_container').html(result);
            initQuoteSelectedCarrier();
            initRateChangeAction();
            $('#popup_loader').hide();
        });
}


//functions
function initQuoteSelectedCarrier() {
    var quote_selected_carrier = $("input[name='quote_selected_carrier']").val();
    var use_quote_packages = $("input[name=use_quote_packages]").is(':checked');
    if(quote_selected_carrier && use_quote_packages) {
        $("input[name=rate][value='"+quote_selected_carrier+"']").prop("checked",true);
        $("input[name=rate]").each(function () {
            if ($(this).val() != quote_selected_carrier) {
                $(this).parent().hide();
            }
        });
    }
    actionRateChanged();
}


function initRateChangeAction() {
    $('#ship_order input[name=rate]').on('change', function() {
        actionRateChanged();
    });
}

function actionRateChanged() {
    var rateId = $('input[name=rate]:checked', '#ship_order').val();
    if (rateId) {
        $('#ship_order_submit').addClass('btn-success');
    } else {
        $('#ship_order_submit').removeClass('btn-success');
    }
}

function initStateOrProvincesOptions(form_id) {
    var canada_provinces_options = '<select><option value="" disabled selected>Select your Province</option><option value="AB">Alberta</option><option value="BC">British Columbia</option><option value="MB">Manitoba</option><option value="NB">New Brunswick</option> <option value="NL">Newfoundland and Labrador</option> <option value="NS">Nova Scotia</option> <option value="ON">Ontario</option> <option value="PE">Prince Edward Island</option> <option value="QC">Quebec</option> <option value="SK">Saskatchewan</option> <option value="NT">Northwest Territories</option> <option value="NU">Nunavut</option> <option value="YT">Yukon</option></select>';
    var us_state_options = '<select class="form-control">  <option value="" disabled selected>Select your State</option>  <option value="AL">Alabama</option>   <option value="AK">Alaska</option>   <option value="AZ">Arizona</option>   <option value="AR">Arkansas</option>   <option value="CA">California</option>   <option value="CO">Colorado</option>   <option value="CT">Connecticut</option>   <option value="DE">Delaware</option>   <option value="DC">District Of Columbia</option>   <option value="FL">Florida</option>   <option value="GA">Georgia</option>   <option value="HI">Hawaii</option>   <option value="ID">Idaho</option>   <option value="IL">Illinois</option>   <option value="IN">Indiana</option>   <option value="IA">Iowa</option>   <option value="KS">Kansas</option>   <option value="KY">Kentucky</option>   <option value="LA">Louisiana</option>   <option value="ME">Maine</option>   <option value="MD">Maryland</option>   <option value="MA">Massachusetts</option>   <option value="MI">Michigan</option>   <option value="MN">Minnesota</option>   <option value="MS">Mississippi</option>   <option value="MO">Missouri</option>   <option value="MT">Montana</option>   <option value="NE">Nebraska</option>   <option value="NV">Nevada</option>   <option value="NH">New Hampshire</option>   <option value="NJ">New Jersey</option>   <option value="NM">New Mexico</option>   <option value="NY">New York</option>   <option value="NC">North Carolina</option>   <option value="ND">North Dakota</option>   <option value="OH">Ohio</option>   <option value="OK">Oklahoma</option>   <option value="OR">Oregon</option>   <option value="PA">Pennsylvania</option>   <option value="RI">Rhode Island</option>   <option value="SC">South Carolina</option>   <option value="SD">South Dakota</option>   <option value="TN">Tennessee</option>   <option value="TX">Texas</option>   <option value="UT">Utah</option>   <option value="VT">Vermont</option>   <option value="VA">Virginia</option>   <option value="WA">Washington</option>   <option value="WV">West Virginia</option>   <option value="WI">Wisconsin</option>   <option value="WY">Wyoming</option></select>';

    var selected_country = $('form#'+form_id+' select#country option:selected').val();
    var selected_state = $('form#'+form_id+' select#state_province option:selected').val();

    if (PREVIOUS_SELECTED_COUNTRY !=  selected_country) {
        if (selected_country == 'Canada') {
            $('form#'+form_id+' select#state_province').html(canada_provinces_options);
        } else if(selected_country == 'United States') {
            $('form#'+form_id+' select#state_province').html(us_state_options);
        }
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
    var re = /^\d{10}$/;
    return re.test(phone);
}

function validateZip(code, country) {
    if (country == 'United States') {
        var re = /^\d{5}$/;
        return re.test(code);
    } else { //canada
        var re = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
        return re.test(code);
    }
}

function searchInTable() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("srch-term");
    filter = input.value.toUpperCase();
    table = document.getElementById("table-orders-list");
    tr = table.getElementsByClassName("accordion-toggle");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        //td = tr[i].getElementsByTagName("td")[0];
        var columns = tr[i].getElementsByTagName("td");

        var find_flag = false;
        for (k = 0; k < columns.length; k++) {
            var current_td = columns[k];

            if (current_td) {
                if (current_td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    find_flag = true;
                }
            }
        }

        if (find_flag) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
    }
}

function lockSetupFormSubmitByEnterKey() {
    var form = $('#f1-address');

    form.bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });
}

function ajaxSendTrackingNumberToCustomer() {
    var shop = $('#btn_send_tracking').data('shop');
    var url = '/orders/send_tracking_number_to_customer?shop='+shop;
    var data = $('form#reference_tracking_number').serialize();
    console.log(data);
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.error != undefined) {
                alert(response.error);
                return false;
            }
            $('#sendTrackingNumber').modal('toggle');
            return true;
        }
    });
}
