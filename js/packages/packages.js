var drawNewRow = function (package) {

    var html = '';

    html += '<tr data-package-id="' + package.id + '">';
    html += '<td>' + package.title + '</td>';
    html += '<td>';
    html += package.length;
    html += 'x';
    html += package.height;
    html += 'x';
    html += package.width;
    html += ' in</td>';
    html += '<td>' + package.weight + ' lb</td>';
    html += '<td>';
    html += '<button class="btn btn-default btn-sm editPackage" onclick="getPackageModal(' + package.id + ');">Edit</button> ';
    html += '<button class="btn btn-danger btn-sm deletePackage" onclick="deletePackage(' + package.id + ');">Delete</button>';
    html += '</td>';
    html += '</tr>';

    return html;

};

var deletePackage = function (id) {

    if (typeof id !== "undefined") {

        var packageId = id;
        var url = '/packages/delete/' + packageId;
        var $currentElement = $('*[data-package-id="' + id + '"]');

        $.ajax({
            type: "DELETE",
            url: url
        })
            .done(function (data) {
                if (data === 'true') {
                    $currentElement.remove();
                }
                window.location.reload();
            });
    }
};

var getPackageModal = function (id) {
    var url = 'packages_popup.html';

    $.ajax({
        type: "GET",
        url: url
    })
        .done(function (html) {
            $('#packageModalBody').html(html);
            $('#newPackageModal').modal();
        });
};
$(document).ready(function () {
    $("#showNewPackageModal").click(function () {

        getPackageModal(0);

    });

    $("#addPackage").click(function () {
        var $form = $('#newPackageForm');
        var packageId = $('input[name="package_id"]').val();
        var data = $form.serialize();
        var url = '/packages/process/';

        $.ajax({
            type: "POST",
            url: url,
            data: data
        })
            .done(function (package) {

                var html = drawNewRow(package);
                if (packageId == 0) {

                    $('#packageList').find('tbody').prepend(html);
                } else {
                    var $currentElement = $('*[data-package-id="' + package.id + '"]');
                    $currentElement.replaceWith(html);
                }
                window.location.reload();

            });

        $form.trigger('reset');
        $('#newPackageModal').modal('hide');

    });

});