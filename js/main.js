$("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

//var openMailInp = false;

var credInputs = $('.form-checkbox-group input');

credInputs.change(function () {

    if (!$(credInputs[0]).prop('checked') && !$(credInputs[1]).prop('checked')) {
        $('.optional-email').hide();
    }else{
        $('.optional-email').show();
    }
});
