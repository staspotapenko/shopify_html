$('#content').on('click', '.j-product-edit-popup', function(){
    $('#j-product-title').text($(this).closest('tr').find('.j-product-title-text').text());
    $('#j-product-id').val($(this).attr('data-product-id'));
    $('#j-product-width').val($(this).attr('data-product-width'));
    $('#j-product-height').val($(this).attr('data-product-height'));
    $('#j-product-length').val($(this).attr('data-product-length'));
    $('#j-product-no-size').prop('checked', $(this).attr('data-product-no-size')==1).attr("disabled", $(this).attr('data-product-pack')==1);
    $('#j-product-pack').prop('checked', $(this).attr('data-product-pack')==1).attr("disabled", $(this).attr('data-product-no-size')==1);
});

$('#content').on('click', '.j-save-product', function () {
    var width = parseInt($('#j-product-width').val());
    var length = parseInt($('#j-product-length').val())
    var height = parseInt($('#j-product-height').val())
    console.log(width, length, height);
    if($('#j-product-no-size').prop('checked')&&(
        width==0 || length==0 || height==0||
        isNaN(width)||isNaN(length)||isNaN(height)
        )) {
        alert('Please enter the dimentions for no box product');
        return false;
    }
    var dataObj = {
        'shopify_product_id':$('#j-product-id').val(),
        'width':$('#j-product-width').val(),
        'height':$('#j-product-height').val(),
        'length':$('#j-product-length').val(),
        'no_size':$('#j-product-no-size').prop('checked'),
        'pack':$('#j-product-pack').prop('checked'),
    };
    $.ajax({
        "type": "POST",
        "url": '/products/products_save',
        "data": dataObj,
        "success": function(data){
            window.location.reload();/*
            var parentObject = $('#product_item_' + dataObj.shopify_product_id).closest('tr');
            var buttonObject = parentObject.find('.j-product-edit-popup');
            buttonObject.attr('data-product-width', dataObj.width);
            buttonObject.attr('data-product-height', dataObj.height);
            buttonObject.attr('data-product-length', dataObj.length);
            buttonObject.attr('data-product-no-size', dataObj.no_size?1:0);
            if(dataObj.no_size){
                parentObject.css('background-color', '#ffffff');
                parentObject.find('.j-item-product-width').html("<b>NO</b>");
                parentObject.find('.j-item-product-height').html("<b>BOX</b>");
                parentObject.find('.j-item-product-length').html("<b>PRODUCT</b>");
            } else {
                console.log(parentObject);
                console.log(parseInt(dataObj.width)>0&&parseInt(dataObj.height)>0&&parseInt(dataObj.length)>0);
                if(parseInt(dataObj.width)>0&&parseInt(dataObj.height)>0&&parseInt(dataObj.length)>0)
                    parentObject.css('background-color', '#ffffff');
                else
                    parentObject.css('background-color', '#ebcccc');
                parentObject.find('.j-item-product-width').html(dataObj.width);
                parentObject.find('.j-item-product-height').html(dataObj.height);
                parentObject.find('.j-item-product-length').html(dataObj.length);
            }*/
        }
    });
});

$('#content').on('change', '#j-product-pack', function(){
    if($(this).is(':checked')){
        $('#j-product-no-size').attr("disabled", true);
    }else {
        $('#j-product-no-size').attr("disabled", false);
    }
});

$('#content').on('change', '#j-product-no-size', function(){
    if($(this).is(':checked')){
        $('#j-product-pack').attr("disabled", true);
    }else {
        $('#j-product-pack').attr("disabled", false);
    }
});